import { noMoney } from "./functions.js";

const preloader = document.querySelector('.preloader');

// Объявляем слушатель событий "клик"
document.addEventListener('click', (e) => {
	let targetElement = e.target;

	if (targetElement.closest('.main__privacy') && preloader.classList.contains('_hide')) {
		preloader.classList.remove('_hide');
	}

	if (targetElement.closest('.preloader__button')) {
		preloader.classList.add('_hide');
	}

	if (targetElement.closest('.bet-box__minus')) {
		const bet = +sessionStorage.getItem('current-bet');
		if (bet > 100) {
			sessionStorage.setItem('current-bet', bet - 50);
			document.querySelector('.bet-box__score p').textContent = sessionStorage.getItem('current-bet');
		}
	}
	if (targetElement.closest('.bet-box__plus')) {
		const bet = +sessionStorage.getItem('current-bet');
		if (+sessionStorage.getItem('money') > bet + 50) {
			sessionStorage.setItem('current-bet', bet + 50);
			document.querySelector('.bet-box__score p').textContent = sessionStorage.getItem('current-bet');
		} else noMoney('.score');
	}
})